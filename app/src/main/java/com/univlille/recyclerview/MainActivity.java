package com.univlille.recyclerview;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

// Exemple simple de RecyclerView.
// Les données seront produites localement de façon simple (pas d'appel à une API ou à une base
// de données par exemple)

public class MainActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;
    private CustomAdapter customAdapter;

    // les données (en dur ici pour faire simple)
    private ArrayList<String> dataSet = new ArrayList<>(Arrays.asList("1er", "2nd", "3rd", "4th", "5th", "6th", "7th", "8th", "9th"));

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupRecyclerView();
    }

    // Création et paramétrage du RecyclerView
    private void setupRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.myRecyclerView);

        // Utilisez ce paramètre pour améliorer les performances si vous savez que les changements
        // du contenu ne modifient pas la taille de la présentation du RecyclerView.
        recyclerView.setHasFixedSize(true);

        // on applique un linear layout manager (c-a-d liste verticale). On aurait pu utiliser
        // un grid layout ou encore un Staggered Grid Layout Manager
        layoutManager = new LinearLayoutManager(this);
        // testez pour voir :
        // layoutManager = new GridLayoutManager(this,2);
        // layoutManager = new GridLayoutManager(this,2, RecyclerView.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

        // on associe notre adapter au RecyclerView et on lui passe les données que l'adapter
        // va gérer
        customAdapter = new CustomAdapter(dataSet);
        recyclerView.setAdapter(customAdapter);
    }
}
