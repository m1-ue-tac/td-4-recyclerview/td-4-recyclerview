package com.univlille.recyclerview;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

// classe externe cette fois-ci poor gérer la vue d'un item
public class MyViewHolder extends RecyclerView.ViewHolder {

    private final TextView textView;
    private ImageView imageView;

    // pour créer la vue d'une ligne dans la liste
    // (appelé par l'adapter)
    public MyViewHolder(@NonNull View itemView) {
        super(itemView);

        // gestion du clic sur un item de la liste
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "clic sur la vue n° " + (getAdapterPosition() + 1), Toast.LENGTH_SHORT).show();
            }
        });

        // gestion du clic sur le premier texte ("1er", "2nd"...) d'un item de la liste
        textView = (TextView) itemView.findViewById(R.id.textItem);
//            textView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Toast.makeText(view.getContext(), "clic sur le texte n° " + (getAdapterPosition() + 1), Toast.LENGTH_SHORT).show();
//                }
//            });

        imageView = itemView.findViewById(R.id.imageView);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "clic sur l'image n° " + (getAdapterPosition() + 1), Toast.LENGTH_SHORT).show();
            }
        });
    }

    // pour accéder aux éléments présents dans la vue de la ligne
    public TextView getTextView() {
        return textView;
    }

    public ImageView getImageView() {
        return imageView;
    }

}
