package com.univlille.recyclerview;

import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

// l'adapter est maintenant relié au viewholderMyViewHolder (ici en classe interne). Les méthodes
// associées ont été mises à jour.

public class CustomAdapter extends RecyclerView.Adapter<MyViewHolder> {

    // les données à faire apparaître dans la liste
    private final ArrayList<String> localDataSet;

    /**
     * Crée l'Adapter et l'initialise avec le dataset
     *
     * @param dataSet String[] contient les données qui rempliront les vues de la RecycleView.
     */
    public CustomAdapter(ArrayList<String> dataSet) {
        this.localDataSet = dataSet;
    }

    // pour créer un viewHolder
    // (appelé par le layout manager du RecyclerView)
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Crée une vue pour une ligne dans la liste et définit son IHM
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.text_row_item_card, parent, false);

        return new MyViewHolder(view);
    }

    // pour lier les données au viewHolder.
    // Met à jour le contenu de la vue
    // (appelé par le layout manager du RecyclerView)
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        // Récupère un élément du dataset à la position indiquée, et remplace le contenu de la
        // vue avec cet élément
        int position2 = holder.getAdapterPosition();
        String data = getLocalDataSet(position2);
        holder.getTextView().setText(data);
        // Maintenant, chaque fois qu'on clique sur le titre de la card, on la supprime
        holder.getTextView().setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                // on supprime réellement la donnée
                removeLocalDataSet(position2);
                // on notifie le RecyclerView pour qu'il supprime la card
                notifyItemRemoved(position2);
                // on notifie le RecyclerView de raffraichir la liste en mettant à jour les positions
                notifyItemRangeChanged(position2, getItemCount());
            }
        });
    }

    private String getLocalDataSet(int position) {
        return localDataSet.get(position);
    }

    private void removeLocalDataSet(int position) {
        localDataSet.remove(position);
    }

    // pour savoir combien de données on doit gérer. On récupère la taille du dataset.
    // (appelé par le layout manager du RecyclerView)
    @Override
    public int getItemCount() {
        return localDataSet.size();
    }


}
